WITH SubcategorySales AS (
    -- Calculate sales for each subcategory from 1998 to 2001
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM t.time_id) AS year,
        SUM(s.amount_sold) AS total_sales
    FROM
        sh.products p
        JOIN sh.sales s ON p.prod_id = s.prod_id
        JOIN sh.times t ON s.time_id = t.time_id
    WHERE
        EXTRACT(YEAR FROM t.time_id) BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, year
),
PreviousYearSales AS (
    -- Calculate sales for the previous year for each subcategory
    SELECT
        prod_subcategory,
        year,
        LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY year) AS previous_year_sales,
        total_sales
    FROM
        SubcategorySales
)
-- Identify subcategories with consistently higher sales from 1998 to 2001 compared to the previous year
SELECT DISTINCT
    prod_subcategory
FROM
    PreviousYearSales
WHERE
    total_sales > COALESCE(previous_year_sales, 0);